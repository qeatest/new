package newpackage;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class driversetup {
	public static WebDriver driver;
	  public WebDriver chromesetup()
	  {
		  System.setProperty("webdriver.chrome.driver","C:\\Chrome Driver 2.38\\chromedriver_win32\\chromedriver.exe");
		ChromeOptions opt = new ChromeOptions();
	  opt.addArguments("--disable-notifications");
		  
		  WebDriver driver=new ChromeDriver(opt);
		  driver.get("https://www.redbus.in");
		  driver.manage().window().maximize();
		 
		  driver.manage().timeouts().implicitlyWait(300,TimeUnit.SECONDS);
		  driver.manage().timeouts().pageLoadTimeout(300,TimeUnit.SECONDS);
		return driver;
	}
}
